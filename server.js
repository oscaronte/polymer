var express = require("express"),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var cors = require('cors');

app.use(cors());
app.use(express.static(__dirname+'/build/default'));

app.get('/', function(req, res) {
  res.sendFile('index.html', {root:'.'});
});

app.listen(port);

console.log('Proyecto Polymer con wrapper de NodeJS en puerto: '+port);
